# test-e2e

Package to demonstrate how to run E2E tests and other Selenium-based processes
from Windows Subsystem for Linux

## System prerequisites

On a Debian-based system:

```bash
# Add Google Chrome deb repository
sudo sh -c 'echo "deb [arch=amd64] https://dl.google.com/linux/chrome/deb/ stable main" > /etc/apt/sources.list.d/google-chrome.list'
wget -q -O - https://dl-ssl.google.com/linux/linux_signing_key.pub | sudo apt-key add -
sudo apt update
sudo apt install -y xvfb google-chrome-stable
```
