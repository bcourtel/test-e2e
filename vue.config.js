module.exports = {
  pluginOptions: {
    critical: {
      penthouse: {
        puppeteer: {
          args: ["--no-sandbox"]
        }
      }
    }
  }
};
